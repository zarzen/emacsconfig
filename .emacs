(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (deeper-blue))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;;设置个人信息
(setq user-full-name "zarzen")
(setq user-mail-address "izarzen@gmail.com")

;;=================some config of WangYin
(setq visible-bell t)
(setq inhibit-startup-message t)
(setq column-number-mode t)
(setq mouse-yank-at-point t)
(setq kill-ring-max 200)
(setq default-fill-column 60)

(setq-default indent-tabs-mode nil)
(setq default-tab-width 4)
(setq tab-stop-list ())
;(loop for x downfrom 40 to 1 do
 ;     (setq tab-stop-list (cons (* x 4) tab-stop-list)))
(setq sentence-end "\\([。！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")
(setq sentence-end-double-space nil)

(setq enable-recursive-minibuffers t)

(setq scroll-margin 3
      scroll-conservatively 10000)
(setq default-major-mode 'text-mode)
(show-paren-mode t)
(setq show-paren-style 'parentheses)
(mouse-avoidance-mode 'animate)
(setq frame-title-format "emacs@%b")
(auto-image-file-mode)
(global-font-lock-mode t)


;; 对这些模式重新配置 enter 键, 使得按 enter 换行时可以自动缩进
(add-hook 'c-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'c-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'c++-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'c++-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'java-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'java-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'scheme-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'scheme-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'perl-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'perl-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'javascript-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'javascript-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'idlwave-mode-hook
          (lambda () 
            (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'f90-mode-hook 
            (lambda ()
                (local-set-key [(return)] 'newline-and-indent)))

;;透明
;;(setf (frame-parameter nil 'alpha) 90)
(set-frame-parameter (selected-frame) 'alpha (list 93 80))
(add-to-list 'default-frame-alist (cons 'alpha (list 93 80)))

(global-set-key [(f8)] 'loop-alpha)  ;;注意这行中的F8 , 可以改成你想要的按键    
    
(setq alpha-list '((97 80) (100 100)))    
    
(defun loop-alpha ()    
  (interactive)    
  (let ((h (car alpha-list)))                    
    ((lambda (a ab)    
       (set-frame-parameter (selected-frame) 'alpha (list a ab))    
       (add-to-list 'default-frame-alist (cons 'alpha (list a ab)))    
       ) (car h) (car (cdr h)))    
    (setq alpha-list (cdr (append alpha-list (list h))))    
    )    
)    

;;显示行号
(require 'linum)
(global-linum-mode t)


;定制C/C++缩进风格
(add-hook 'c-mode-hook
          '(lambda ()
             (c-set-style "k&r")))
(add-hook 'c++-mode-hook
          '(lambda ()
             (c-set-style "stroustrup")))
;; 设置缩进字符数
(setq c-basic-offset 4)



;;lisp
(setq inferior-lisp-program "/usr/local/bin/sbcl")
(add-to-list 'load-path "~/.emacs.d/slime")
(load-file "~/.emacs.d/slime/slime.el")
;;(require 'slime)
;;(slime-setup)
(slime-setup '(slime-repl))
(slime-setup '(slime-fancy))

;;edit in chrome
(add-to-list 'load-path "~/.emacs.d")
(require 'edit-server)
(edit-server-start)

;;ibus-mode
(add-to-list 'load-path "~/.emacs.d/ibus-el")
(require 'ibus)

(add-hook 'after-init-hook 'ibus-mode-on)
(global-set-key (kbd "C-;") 'ibus-toggle) ;;这里既是绑定上面设置的C+=快捷键到ibus中
(ibus-define-common-key ?\C-\s nil)
;; Use C-/ for Undo command
(ibus-define-common-key ?\C-/ nil)
;; Change cursor color depending on IBus status
(setq ibus-cursor-color '("red" "blue" "limegreen"))

;;auto-complete
(add-to-list 'load-path "~/.emacs.d/plugins/auto-complete")
(require 'auto-complete)
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/plugins/auto_complete//ac-dict")
(ac-config-default)

;;yasnippet
;(add-to-list 'load-path
;             "~/.emacs.d/plugins/yasnippet")
;(require 'yasnippet)
;(yas-global-mode 1)

;; 对这些模式重新配置 enter 键, 使得按 enter 换行时可以自动缩进
(add-hook 'c-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'c-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'c++-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'c++-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'java-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'java-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'scheme-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'scheme-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'perl-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'perl-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'javascript-mode-hook (lambda () (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'javascript-mode-hook (lambda () (setq comment-column 48) ))
(add-hook 'idlwave-mode-hook
          (lambda () 
            (local-set-key [(return)] 'newline-and-indent) ))
(add-hook 'f90-mode-hook 
            (lambda ()
                (local-set-key [(return)] 'newline-and-indent)))

;;cedet
;(load-file "/usr/share/emacs/24.2.50/lisp/cedet/common/cedet.el")
;(global-ede-mode 1)                      ; Enable the Project management system
;(semantic-load-enable-code-helpers)      ; Enable prototype help and smart completion 
;(global-srecode-minor-mode 1)  


;;color-theme
(add-to-list 'load-path
	     "~/.emacs.d/color-theme")
(require 'color-theme)
(add-to-list 'load-path
	     "~/.emacs.d/emacs-color-theme-solarized")
(require 'color-theme-solarized)
(color-theme-solarized-dark)

;(add-to-list 'load-path
;	     "~/.emacs.d/php-mode")
;(require 'php-mode)
;;根据文件扩展名自动php-mode
;(add-to-list 'auto-mode-alist '("\\.php[34]?\\'\\|\\.phtml\\'" . php-mode))
;;开发项目时，php源文件使用其他扩展名
;(add-to-list 'auto-mode-alist '("\\.module\\'" . php-mode))
;(add-to-list 'auto-mode-alist '("\\.inc\\'" . php-mode))

;;el-get config
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(el-get 'sync)

;;golden-ratio
(add-to-list 'load-path
             "~/.emacs.d/plugins/golden-ratio.el")
(require 'golden-ratio)
(golden-ratio-enable)

;;jade-mode
(add-to-list 'load-path "~/.emacs.d/plugins/jade-mode")
(require 'sws-mode)
(require 'jade-mode)    
(add-to-list 'auto-mode-alist '("\\.styl$" . sws-mode))
(add-to-list 'auto-mode-alist '("\\.jade$" . jade-mode))

;;格式化整个文件函数
(defun indent-buffer ()
"Indent the whole buffer."
(interactive)
(save-excursion
  (indent-region (point-min) (point-max) nil)))
;;绑定到F7键
(global-set-key [f7] 'indent-whole)

;;clang-auto-complete
(add-to-list 'load-path "~/.emacs.d/plugins/auto-complete-clang")
(require 'auto-complete-clang)  
(setq ac-clang-auto-save t)  
(setq ac-auto-start t)  
(setq ac-quick-help-delay 0.5)  
;; (ac-set-trigger-key "TAB")  
;; (define-key ac-mode-map  [(control tab)] 'auto-complete)  
(define-key ac-mode-map  [(control tab)] 'auto-complete)  
(defun my-ac-config ()  
  (setq ac-clang-flags  
        (mapcar(lambda (item)(concat "-I" item))  
               (split-string  
                "  
 /usr/lib/gcc/x86_64-redhat-linux/4.7.2/../../../../include/c++/4.7.2
 /usr/lib/gcc/x86_64-redhat-linux/4.7.2/../../../../include/c++/4.7.2/x86_64-redhat-linux
 /usr/lib/gcc/x86_64-redhat-linux/4.7.2/../../../../include/c++/4.7.2/backward
 /usr/lib/gcc/x86_64-redhat-linux/4.7.2/include
 /usr/local/include
 /usr/include

")))  
  (setq-default ac-sources '(ac-source-abbrev ac-source-dictionary ac-source-words-in-same-mode-buffers))  
  (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup)  
  ;; (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)  
  (add-hook 'ruby-mode-hook 'ac-ruby-mode-setup)  
  (add-hook 'css-mode-hook 'ac-css-mode-setup)  
  (add-hook 'auto-complete-mode-hook 'ac-common-setup)  
  (global-auto-complete-mode t))  
(defun my-ac-cc-mode-setup ()  
  (setq ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources)))  
(add-hook 'c-mode-common-hook 'my-ac-cc-mode-setup)  
;; ac-source-gtags  
(my-ac-config)  

;;去掉toolbar
(tool-bar-mode 0)

